let 
  nixpkgs = import <nixpkgs> {};
  patchedPkgsSrc = nixpkgs.stdenv.mkDerivation {
    name = "patchedPkgsSrc";
    src = nixpkgs.pkgs.fetchFromGitHub {
      owner = "nixos";
      repo = "nixpkgs-channels";
      rev = "81fceb255448415e70b9e7775d590b6def45f861";
      sha256 = "0sfx21b9rb6qxjm7li3krk6ik0xxph1il7r5l69n8b9agp72yjfx";
#      rev = "1b1fc6550559f9d73ddf7cea611c387a847bf03b";
#      sha256 = "0gipwxghvwnv2n7csp8ks3l2g1z7hwqn96bljikkm7p8jjpfb5ds";
    };
    patches = [ 
      #./build/texlive-url-mirror.patch
      ./build/update-minted-2.5.patch
    ];
    buildPhase = "";
    installPhase = ''
      cp -a . $out
    '';
    fixupPhase = "true";
    postFixup = "true";
  };
  pkgs = import patchedPkgsSrc {};

  mytexlive =  (pkgs.texlive.combine {
    inherit (pkgs.texlive) scheme-full minted pygmentex fvextra;
  });

in pkgs.stdenv.mkDerivation {
  name = "msc-thesis";
  shellHook = ''
    alias vim-thesis="vim -p --servername VIM -- \
      $PWD/src/docs/thesis.tex \
      $PWD/src/docs/glossary.tex \
      $PWD/src/docs/parts/context/context.tex \
      $PWD/src/docs/parts/research_and_development/research_and_development.tex \
      $PWD/src/docs/parts/eval_and_conclusion/eval_and_conclusion.tex \
    "
  '';
  buildInputs = [ 
    pkgs.xdotool
    (pkgs.vim_configurable.customize {
      name = "vim"; 
      vimrcConfig = {
        
         customRC = ''
            source .vimrc
         '';

         vam.knownPlugins = pkgs.vimPlugins;
         vam.pluginDictionaries = [
           "youcompleteme"
           "vim-airline"
           "ctrlp"
           "vim-css-color"
           "rainbow_parentheses"
           "vim-colorschemes"
           "vim-signify"
           "vimtex"
        ];
      };
    })
    pkgs.bashInteractive

    mytexlive
    pkgs.biber
    pkgs.pygmentex
    pkgs.python27Packages.pygments-markdown-lexer

    pkgs.zathura
  ]; 
}
