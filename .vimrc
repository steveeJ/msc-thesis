set nocompatible

" leader
let mapleader = '\'

" save on ctrl-s
nnoremap <c-s> :w<CR>
inoremap <c-s> <Esc>:w<CR>

set hidden
syntax on
set hlsearch
set number
set relativenumber

" mappings to stop insert mode
imap jjj <ESC>
imap kkk <ESC>
imap lll <ESC>
imap hhh <ESC>
set scroll=11

" new scroll mappings
noremap <C-j> <C-f>
noremap <C-k> <C-u>

noremap <C-n> :tabn<CR>
noremap <C-p> :tabp<CR>
" TODO: get tab movement working with ctrl-alt-{n,p}
noremap <C-A-n> :tabm +1<CR>
noremap <C-A-p> :tabm -1<CR>
let g:ctrlp_map = '<tab>'
let g:ctrlp_custom_ignore = {
\ 'dir':  '\v[\/]\.(git|hg|svn|)$$',
\ 'file': '\v\.(exe|so|dll|so|swp|zip|aux|log|fdb_latexmk|fdb|dvi|lof|lot|pdf|fls|toc|gz|latexmain)$$',
\ }

" allways show status line
set ls=2
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
"set textwidth=80

" sync default register to clipboard {
if has('unnamedplus')
set clipboard=unnamedplus
else
set clipboard=unnamed
endif
" }

" colored brackets {
let g:rbpt_colorpairs = [
  \ ['brown',       'RoyalBlue3'],
  \ ['Darkblue',    'SeaGreen3'],
  \ ['darkgray',    'DarkOrchid3'],
  \ ['darkgreen',   'firebrick3'],
  \ ['darkcyan',    'RoyalBlue3'],
  \ ['darkred',     'SeaGreen3'],
  \ ['darkmagenta', 'DarkOrchid3'],
  \ ['brown',       'firebrick3'],
  \ ['gray',        'RoyalBlue3'],
  \ ['black',       'SeaGreen3'],
  \ ['darkmagenta', 'DarkOrchid3'],
  \ ['Darkblue',    'firebrick3'],
  \ ['darkgreen',   'RoyalBlue3'],
  \ ['darkcyan',    'SeaGreen3'],
  \ ['darkred',     'DarkOrchid3'],
  \ ['red',         'firebrick3'],
  \ ]
let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces
" }
set backspace=indent,eol,start
colorscheme PaperColor

" Latex Related {{{
au BufRead,BufNewFile *.tex,*.md,*.markdown setlocal spell spelllang=en_us

let g:vimtex_view_method = 'zathura'
let g:vimtex_complete_enabled = 1
let g:vimtex_complete_close_braces = 1
let g:vimtex_complete_recursive_bib = 1
let g:vimtex_indent_enabled = 1
let g:vimtex_indent_bib_enabled = 1
let g:vimtex_fold_enabled = 1
let g:vimtex_fold_comments = 1
let g:vimtex_fold_preamble = 1

let g:vimtex_latexmk_options = '-verbose -pdf -shell-escape -file-line-error -synctex=1 -interaction=nonstopmode'

if !exists('g:ycm_semantic_triggers')
    let g:ycm_semantic_triggers = {}
endif
let g:ycm_semantic_triggers.tex = [
    \ 're!\\[A-Za-z]*cite[A-Za-z]*(\[[^]]*\]){0,2}{[^}]*',
    \ 're!\\[A-Za-z]*ref({[^}]*|range{([^,{}]*(}{)?))',
    \ 're!\\hyperref\[[^]]*',
    \ 're!\\includegraphics\*?(\[[^]]*\]){0,2}{[^}]*',
    \ 're!\\(include(only)?|input){[^}]*',
    \ 're!\\\a*(gls|Gls|GLS)(pl)?\a*(\s*\[[^]]*\]){0,2}\s*\{[^}]*',
    \ 're!\\includepdf(\s*\[[^]]*\])?\s*\{[^}]*',
    \ 're!\\includestandalone(\s*\[[^]]*\])?\s*\{[^}]*',
    \ ]

function! ViewerCallback() dict
call self.forward_search(self.out)
endfunction
let g:vimtex_view_zathura_hook_callback = 'ViewerCallback'
" }}}

" hack to copy mendeley bibliography
autocmd BufWritePost * execute ':silent ! cp /home/steveej/src/mendeley/Static-Code-Analysis-Kernel-Memory-Saftey.bib /home/steveej/src/steveej/msc-thesis/src/docs/thesis.bib >/dev/null 2>&1'
