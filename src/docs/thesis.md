% From Declarative Source-Package Management to Application Container Deployment
% Stefan Junker
% Wintersemester 2016/2017

* Source-Based Build System for Application Containers
* From Declarative Source-Package Management to Application Container Deployment
* Container Build System using Source-Package Management
* Using A Declarative And Deterministic Source-Package Manager to Build Application Container Images

# Introduction To Topic And The Underlying Problem
(...)

## Basic Terms and Relations
Before solving the underlying problem, I want to give a brief summary of what I'm referring to as packages, containers, application and their source. Further enlarging upon the definitions will follow in succeeding chapters when more detailed knowledge will be required.

### Application
In the context of computers and their operating systems, an application consists of a unique set of files that are required to execute that specific application on the running operating system.
The operating system understands how to execute the application's files, which are binary translations from the application source code.

For reasons that will be discussed in a later chapter, almost every application is written to depend on library functions that are not part of the application itself, but need to be properly installed on the operating system for the applications to find and use it.

### Package
In order to install an run application and library files on different systems, their set of files are packed into single files.
These are called packages, and can be transferred to other computers and installed there via the operating systems package manager.

### Application Container
If an application and all it's needed library function files are instead packed together into one file it can be called an application container image.

TODO: explain the different understandings of what a container is and what is relevant for this thesis

#### Chroot
On Linux, the syscall _chroot()_ will change the effective root directory for the current process.
TODO

#### Namespaces
TODO

#### Cgroup

## Ideology versus Reality
In the ideal case it would be possible to install an application and its required shared library files in every possible combination on the same computer, which as an example could mean to install many different applications, multiple versions of the same application and libraries and of course a combination.
It would also not cause a conflict or side effects if the shared libraries, that are required by different applications, have to be installed in multiple different versions.

Unsurprisingly the reality is not ideal, at least not for many popular operating systems and their included package management systems.


### Classical (Non Container) Attempts
Developers of operating systems and package managers have been trying to achieve this ideal situation for a long time.
The most widely spread Operating Systems and their package managers still suffer from the described problem

* TODO: statistics about OSs in use

* TODO bare-metal OS installations
* TODO packages installed and run in the top OS layer
* TODO Application and dependency deployment
* TODO Pick up on explain package conflict. 
* TODO Application life cycle management
* TODO explain how application containers provide an interface between the system and the application
* TODO container life cycle management

* TODO: the thesis does not cover isolation requirements for improved security

## Human Roles and Interaction
* TODO: operating system developer
* TODO: system administrator
* TODO: system architect
* TODO: application developer
* TODO: application packager
* TODO: interfaces and interactions
    * OS configuration -> application agnostic?
    * Network topology management -> isolation of networking?

* TODO rewrite:
    Typically the system administrator doesn't necessarily think about the work-flow of an application developer and vise versa.
    The system architect unfortunately isn't oftentimes involved in the decision making of the administrator and application developer. 
    This circumstance can and eventually will lead to applications running on systems that have undergone severe workarounds to make it work at all.
    The DevOps movement, which is a modern term and attempt to combine the three aforementioned three roles, has been trying to solve the described problem with tools for automatic system configuration.

## Personal Role Experiences
I've been able to collect first-hand experience in multiple of the aforementioned roles, which allowed me to create a very broad view of use-cases and their respective requirements in which containers can be used to improve the overall situation.

The most recent project that I want to name is called [embEDUx][embEDUx], and it aims to provide a flexible solution for creating and deploying a customized Linux distribution for embedded systems in an educational context.
My main part within that project was to design the overall architecture of the build system and to develop the specification for the system archive and the method of processing it to a complete system archive. 
Additional experience in this area comes from my work on projects for customers that consulted me to advise and administer their IT small-business infrastructure.

## Intention
* TODO: evaluate the existing build systems, their design choices and resulting features and methodologies. 
The intention for customizing and adapting each one of them to make them suitable for two types of users, system administrators and application developers.

# Design Specification
## Scope
* Package format
* Creating
* Package selection
* Post-Install commands


# Pre-Evaluation
* TODO: explain and mention 12-factor apps

## Criteria
* Usability 
* Correctness

## Automated System Administration
* puppet
* chef
* ansible

## Build Systems
* Package Mangers
    * Portage
    * Nix (and NixOS)
* Embedded Targeted
    * embEDUx' buildrootfs routine
    * baserock
    * yocto
    * openembedded
    * buildroot
* Distribution Release tools
    * Debian - https://wiki.debian.org/ReproducibleBuilds
    * Nix
    * ChromeOS/CoreOS SDK
    * Gentoo Catalyst
* Docker related
    * docker build (Dockerfile)
* ACI related
    * acbuild

## Runtimes

* Docker
* rkt

## Deployment

### Nix + rkt


# Discussion
* Distributing
* Discovering
* Running

# Terminology

* Application: TODO
* Container: TODO
* Application Container: TODO
* Application Container Image: TODO
* DevOps: TODO

# References
[embEDUx]: https://embedux.github.io/documentation
