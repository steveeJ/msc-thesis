% // vim: set ft=tex:
\documentclass[12pt,a4paper]{report}

%\overfullrule=1cm

\usepackage[utf8]{inputenc}

\usepackage{titlesec} % titleformat
\usepackage{blindtext,color,fancyhdr}

\usepackage{geometry}
\geometry{a4paper, top=25mm, left=35mm, right=30mm,  bottom=35mm, headsep=10mm, footskip=12mm}

\usepackage{multirow,tabularx,tabu}
\usepackage{booktabs}
\usepackage{spreadtab}
\usepackage{hhline}
\renewcommand{\arraystretch}{1.2}

\usepackage{colortbl}
\usepackage[dvipsnames]{xcolor}

\usepackage[backend=biber,style=numeric,citestyle=numeric,url=true]{biblatex}
\addbibresource{thesis.bib}

%\usepackage[hyphens]{url}
\Urlmuskip = 0mu plus 1mu

%\hyphenpenalty=1
\pretolerance=5000
\tolerance=5000
%\exhyphenpenalty=1

\usepackage[numberedsection,toc,numberline,nopostdot]{glossaries}
\makenoidxglossaries

\usepackage{listings}
\providecommand*{\listingautorefname}{Listing}
\usepackage{minted}
\usepackage{graphicx}
\usepackage{placeins}
\usepackage{tikz}
\usetikzlibrary{tikzmark,mindmap}
\usetikzlibrary{chains,shapes.arrows, arrows, positioning,decorations.pathreplacing,bending}
\usetikzlibrary{calc}
\usetikzlibrary{matrix,shapes,arrows,positioning}
\usetikzlibrary{shapes.geometric, arrows}
\usetikzlibrary{chains,arrows.meta,decorations.pathmorphing,quotes}
\usepackage{smartdiagram}
\usesmartdiagramlibrary{additions}
\usepackage{color}
\usepackage[definitionLists,hashEnumerators,smartEllipses,hybrid]{markdown}
%\usepackage[fencedCode,inlineFootnotes,citations,definitionLists,hashEnumerators,smartEllipses,hybrid]{markdown}


\tikzset{/minted/basename/.initial=minted}
\appto\theFancyVerbLine{\tikzmark{\pgfkeysvalueof{/minted/basename}\arabic{FancyVerbLine}}}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{wrapfig}
\usepackage[parfill]{parskip}

\newcommand{\topic}{Guarantees On In-Kernel Memory-Safety Using Rust's Static Code Analysis}
\newcommand{\authorOne}{Stefan Junker}
\newcommand{\authorOneBirthDate}{23.12.1986}
\newcommand{\authorOneBirthCity}{Rottweil}
\newcommand{\authorOneInit}{SJ}
\newcommand{\authorOnestreet}{Alemannenstr. 7}
\newcommand{\authorOnezip}{78467}
\newcommand{\authorOnecity}{Konstanz}
\newcommand{\authorOneCountry}{Germany}
\newcommand{\authorOneId}{283751}
\newcommand{\supervisorOne}{Prof. Dr. Michael Mächtel}
\newcommand{\supervisorTwo}{Felix Schuckert}
\newcommand{\studies}{Information Technology - Embedded And Mobile Systems}
\newcommand{\startdate}{2017/4/1}
\newcommand{\submitdate}{2017/9/29}
\newcommand{\buzzwords}{memory-safety, operating system development, rust, static software analysis, software vulnerability}
\renewcommand{\abstract}{%
This study evaluated Rust's guarantees on memory safety in the OS through static analysis.
Static analysis was identified as a requirement due to the assumption that humans tend to make mistakes and C was found to be an error prone language.
Rust, as an affine-typed borrow- and lifetime-checked systems language that is equipped with an ownership model, is considered a viable candidate for replacing C for today's OS development.
After identifying common weaknesses of memory vulnerabilities and their manifestations, the choice of programming language was verified as the most effective mitigation attempt.
Rust was chosen to act as a new candidate, and was found to be effective against common errors in buffer handling due to its ownership model and strong type system.
The language shown to be less error prone in memory intensive tasks like buffer handling, which has been identified as a common cause in software vulnerabilities.
After experimentation with various stack protection scenarios and implementing preemptive multitasking on top of interemezzOS, the downside was discovered that Rust cannot statically detect stack overflows of any type.
Implementing this detection was considered beyond scope, although requirements could be identified for future reference.
It was concluded that Rust's static checks of all kinds are a big improvement in OS development, where object orientation and other paradigms are not simple to manage.
Despite the failed hypotheses of guaranteeing full memory-safety in OS development, Rust is suggested as the language for today's and future OS development.
}

% Numbered Subsubsections
\setcounter{secnumdepth}{3}

\date{}
\title{\topic}

\author{authorOne}

\usepackage{etoolbox}
\patchcmd{\part}{\thispagestyle{plain}}{\thispagestyle{empty}}
  {}{\errmessage{Cannot patch \string\part}}

\fancypagestyle{empty}{%
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}
    \fancyhf{}
}

\fancypagestyle{front}{%
    \pagenumbering{roman}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}
    \fancyhf{}
}

\fancypagestyle{preamble}{%
    \pagenumbering{roman}
    \renewcommand{\headrulewidth}{0pt}
    \renewcommand{\footrulewidth}{0pt}
    \fancyhf{}
    \fancyfoot[R]{\thepage{}}
}

\fancypagestyle{main}{%
    \pagenumbering{arabic}
    \renewcommand{\headrulewidth}{0.25pt}
    \renewcommand{\footrulewidth}{0.25pt}
    \fancyhf{}
    \addtolength{\headwidth}{\marginparsep}
    \headheight=15pt
    \fancyhead[L]{\footnotesize\nouppercase{\leftmark}}
    \fancyhead[C]{}
    \fancyhead[R]{}
    \fancyfoot[R]{\thepage{}}
}

\titlespacing*{\chapter}{0cm}{-1cm}{0.75cm}
\titleformat{\chapter}[hang]{\normalfont\Large\bfseries}{\thechapter}{0.5cm}{}

\usepackage{hyperref}
\usepackage{cleveref}

\makeatletter

\newcommand{\cnameref}[1]{\cref{#1} \textit{(\nameref{#1})}}
\newcommand{\Cnameref}[1]{\Cref{#1} \textit{(\nameref{#1})}}

\newcommand{\cpnameref}[1]{\cref{#1} \textit{(\nameref{#1}, \cpageref{#1})}}
\newcommand{\Cpnameref}[1]{\Cref{#1} \textit{(\nameref{#1}, \cpageref{#1})}}

\newcommand{\cnamepref}[1]{\cref{#1} \textit{(\nameref{#1}, \cpageref{#1})}}
\newcommand{\Cnamepref}[1]{\Cref{#1} \textit{(\nameref{#1}, \cpageref{#1})}}

%\renewcommand\paragraph{\startsection{paragraph}{4}{\z}%
%                                     {-3.25ex\plus -1ex \minus -.2ex}%
%                                     {0.0001pt \plus 0.2ex}%
%                                     {\normalfont\normalsize\bfseries}}
%\renewcommand\subparagraph{\startsection{subparagraph}{5}{\z}%
%                                     {-3.25ex\plus -1ex \minus -.2ex}%
%                                     {0.0001pt \plus 0.2ex}%
%                                     {\normalfont\normalsize\bfseries}}

\newcommand{\iitemA}{\setlength\itemindent{0pt}\item}
\newcommand{\iitemB}{\setlength\itemindent{25pt}\item}
\newcommand{\iitemC}{\setlength\itemindent{50pt}\item}

\let\Partmark\partmark
\def\partmark#1{\def\Partname{#1}\Partmark{#1}}
\let\Chaptermark\chaptermark
\def\chaptermark#1{\def\Chaptername{#1}\Chaptermark{#1}}
\let\Sectionmark\sectionmark
\def\sectionmark#1{\def\Sectionname{#1}\Sectionmark{#1}}
\let\Subsectionmark\subsectionmark
\def\subsectionmark#1{\def\Subsectionname{#1}\Subsectionmark{#1}}
\let\Subsubsectionmark\subsubsectionmark
\def\subsubsectionmark#1{\def\Subsubsectionname{#1}\Subsubsectionmark{#1}}


\newenvironment{compactminted}{%
    \VerbatimEnvironment
    \let\FV@ListVSpace\relax
    \begin{minted}}%
    {\end{minted}}

\tikzset{west above/.code=\tikz@lib@place@handle@{#1}{south west}{0}{1}{north west}{1}}
\tikzset{west below/.code=\tikz@lib@place@handle@{#1}{north west}{0}{-1}{south west}{1}}
\tikzset{east above/.code=\tikz@lib@place@handle@{#1}{south east}{0}{1}{north east}{1}}
\tikzset{east below/.code=\tikz@lib@place@handle@{#1}{north east}{0}{-1}{south east}{1}}

% Tikzmark code helpers
\newcommand{\tikzmarkprefix}{\pgfkeysvalueof{/tikz/tikzmark prefix}}
\newcommand{\tikzmarkcountprep}[1]{%
\tikzset{tikzmark prefix=#1}%
\newcounter{Tikzcounter#1}%
\setcounter{Tikzcounter#1}{0}%
}
\newcommand{\tikzmarkcount}[1][\tikzmarkprefix]{%
\stepcounter{Tikzcounter#1}%
\tikzmark{\arabic{Tikzcounter#1}}%
}
\newcommand{\tikzmarkgetcount}[1][\tikzmarkprefix]{%
\expandafter\arabic\expandafter{Tikzcounter#1}%
}
\newcommand{\tikzmarkcircle}[1]{%
\tikz[baseline=-0.77ex]\fill circle[fill=black,radius=1.1ex] node[font=\small,color=white]{#1};%
}
\newcommand{\tikzmarkdrawcirclesarg}[1]{%
    \begin{tikzpicture}[remember picture,overlay]
        \foreach \x in {1,...,\expandafter\arabic{Tikzcounter#1}}
            \fill (pic cs:\x)+(1.3ex,0.5ex) circle[fill=black,radius=1.1ex,anchor=west] node[font=\small,color=white]{$\x$};
    \end{tikzpicture}%
}
\newcommand{\tikzmarkdrawcircles}{%
    \begin{tikzpicture}[remember picture,overlay]
        \foreach \x in {1,...,\expandafter\arabic\expandafter{Tikzcounter\expandafter\tikzmarkprefix}}
        \fill (pic cs:\x)+(1.3ex,0.5ex) circle[fill=black,radius=1.1ex,anchor=west] node[font=\small,color=white]{$\x$};
    \end{tikzpicture}%
}

% capitablize every First Letter
\let\oldmakefirstuc\makefirstuc
\renewcommand*{\makefirstuc}[1]{%
  \def\gls@add@space{}%
  \mfu@capitalisewords#1 \@nil\mfu@endcap
}
\def\mfu@capitalisewords#1 #2\mfu@endcap{%
  \def\mfu@cap@first{#1}%
  \def\mfu@cap@second{#2}%
  \gls@add@space
  \oldmakefirstuc{#1}%
  \def\gls@add@space{ }%
  \ifx\mfu@cap@second\@nnil
    \let\next@mfu@cap\mfu@noop
  \else
    \let\next@mfu@cap\mfu@capitalisewords
  \fi
  \next@mfu@cap#2\mfu@endcap
}
\newcommand{\code}[2][md]{\mintinline{#1}{`#2`}}

\makeatother
\include{glossary}


\begin{document}

    \include{cover}
\pagestyle{front}
    \include{title}

\pagestyle{preamble}
    \include{abstract}
    \include{affidavit}
    \cleardoublepage
    \newcounter{roman_pagenumbers} % save page number
    \setcounter{roman_pagenumbers}{\value{page}}

\pagestyle{main}
	%TODO \include{acknowledgments}

	\tableofcontents

    \part{Context}
    \label{context}

    \printnoidxglossary

    \include{parts/context/context}

    \part{Research And Development}
    \label{rnd}
    \include{parts/research_and_development/research_and_development}

    \part{Evaluation And Conclusion}
    \label{enc}
    \include{parts/eval_and_conclusion/eval_and_conclusion}

	\newpage
	%TODO \listofmyequations
	\listoftables
	%TODO \lstlistoflistings
	\listoffigures
    \printbibliography

\end{document}
