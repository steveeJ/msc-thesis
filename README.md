# Using Static Code Analysis To Improve Kernel-Task Memory-Safety

# Simple Motivational Golden Circle
* Describes what this project is about very briefly
* Helps to keep track of the goals

## Who?
* Primariliy: OS Software Developers
* Secondary: End Users

## Why?
* The Linux/C/C++ software is full of vulnerabilities
* A human solution is not reliable:
    - Developers make mistakes anyway!
* C/C++ Compilers allow too many mistakes
* Hardware-supported memory-management with stack/heap is hard to implement safely
* Rust is a promising language

## How?
* Look for a *technical* solution for these issues by first analyzing the status quo:
    - Which popular systems are most vulnerable?
    - What exact software situations cause these vulnerabilities?
* Look through CVE database

## What?
* Find out if Rust can help with memory safety within the OS
